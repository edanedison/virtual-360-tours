import React from 'react';
import 'aframe';
import {Entity, Scene} from 'aframe-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import reactCSS from 'reactcss';

class DefaultLayout extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      isLoaded: false
    };

    this.onSomething = this.onSomething.bind(this);
  }


  onSomething() {
    return this.setState({
      isLoaded: true
    });
  }

  render() {

    const {actions} = this.props;
    return (
      <div style={{position: 'absolute', height: '100%', width: '100%'}}>
      <Scene> 
        {this.props.children}
      </Scene>
      </div>
      );
  }
}

DefaultLayout.propTypes = {
  children: React.PropTypes.object,
  actions: React.PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      // signOut
    }, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultLayout);
