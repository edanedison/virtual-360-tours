import 'aframe';

import {Entity, Scene} from 'aframe-react';

import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {bindActionCreators} from 'redux';

import Camera from '../../components/Camera';
import Sky from '../../components/Sky';
import Text from '../../components/Text';

import images from '../../config/images';

const colors = ['red', 'blue', 'green'];

const scenes = ['living room', 'hallway'];
const sceneImages = [images.livingRoom, images.hallway];

const beacons = [
    ['1 0 -3', '0 0 4'], // scene 1
    ['-4 0 1.5'] // scene 2
];

      let povs = [];

class Tour extends React.Component {

  constructor(props) {
      super(props);
      this.state = {
          scene: scenes[0],
          sceneImage: sceneImages[0],          
          cameraLocation: '0 0 2',
          cameraRotation: '0 0 0',
          beaconColor: 'red',
          beaconRadius: 0.25
      };

      this.teleport = this.teleport.bind(this);
  }

  
  componentWillMount() {
    
  console.log('component will mount..');
  
    for (let sceneIndex = 0; sceneIndex < sceneImages.length; sceneIndex++) {
      for (let beaconIndex = 0; beaconIndex < beacons[sceneIndex].length; beaconIndex++) {
       console.log("scene: " + sceneIndex + " beacon: " + beaconIndex);
        povs.push(
          <a-sphere 
            key={beacons[sceneIndex][beaconIndex]}          
            color={this.state.beaconColor} 
            radius={this.state.beaconRadius} 
            position={beacons[sceneIndex][beaconIndex]} 
            onClick={(e) => this.teleport(sceneIndex, beaconIndex)}/>
        );
      }  
    }     
    
  }
  
  
  componentWillUpdate() {
    
  povs = [];
  
    for (let sceneIndex = 0; sceneIndex < sceneImages.length; sceneIndex++) {
      for (let beaconIndex = 0; beaconIndex < beacons[sceneIndex].length; beaconIndex++) {
       console.log("scene: " + sceneIndex + " beacon: " + beaconIndex);
        povs.push(
          <a-sphere 
            key={beacons[sceneIndex][beaconIndex]}
            color={this.state.beaconColor} 
            radius={this.state.beaconRadius} 
            position={beacons[sceneIndex][beaconIndex]} 
            onClick={(e) => this.teleport(sceneIndex, beaconIndex)}/>
        );
      }  
    }     
    
  }  
  

  teleport(sceneId, beaconId) {
      return this.setState({
          scene: scenes[sceneId],
          sceneImage: sceneImages[sceneId],
          cameraLocation: beacons[sceneId][beaconId],
          color: colors[Math.floor(Math.random() * colors.length)]
      });
  }
    

  render() {
    
  console.log(this.state.scene);
    
    return (
      <Entity>
        
        <Camera rotation={this.state.cameraRotation} position={this.state.cameraLocation}>
          <a-cursor fuse={true} fuse-timeout={500}>
          </a-cursor>
        </Camera>

        <Sky src={`url(${this.state.sceneImage})`}/>

        {povs}

      </Entity>
    );
  }
}

Tour.propTypes = {
  actions: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
  };
}


function mapDispatchToProps(dispatch) {
  return {
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(Tour);
