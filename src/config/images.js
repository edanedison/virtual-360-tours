const images = {
	livingRoom: require('../images/living-room.jpg'),
	functionRoom: require('../images/function-room.jpg'),
	hallway: require('../images/hallway.jpg')		
}

export default images