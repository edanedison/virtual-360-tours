import React from 'react';
import {Route, IndexRoute} from 'react-router';

import Default from '../layouts/Default';

import Tour from '../routes/tour';

export default function Routes(store) {

  return (
  <Route path="/" component={Default} >
    <IndexRoute component={Tour} />
    <Route path="tour" component={Tour} />
  </Route>
  );
}

