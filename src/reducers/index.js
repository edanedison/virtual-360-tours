import {combineReducers} from 'redux';
import { routerReducer } from 'react-router-redux';
import view from './viewReducer';

const rootReducer = combineReducers({
  routing: 
  routerReducer,
  view
});

export default rootReducer;
