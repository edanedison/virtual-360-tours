import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function vrReducer(state = initialState.view, action) {
  switch (action.type) {
    case types.SWITCH_VIEW_SUCCESS:
      return Object.assign({}, state, {
        isLoading: true
      });
    default:
      return state;
  }
}
